CREATE TABLE IF NOT EXISTS `Pokemons` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  
  KEY `pokemon_id` (`id`),
  KEY `pokemon_name` (`name`),
  CONSTRAINT uk_id_name Unique(`id`, `name`)  
);


INSERT IGNORE INTO `Pokemons` (`id`, `name`)
VALUES
  (1, 'Bulbasaur'),
  (2,'Ivysaur'),
  (3,'Venusaur'),
  (4,'Charmander'),
  (5,'Charmeleon'),
  (6,'Charizard'),
  (7,'Squirtle'),
  (8,'Wartortle'),
  (9,'Blastoise');