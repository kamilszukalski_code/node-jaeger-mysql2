#!/bin/bash
docker rm jaeger-node-mysql-demo --force --volumes
docker rm jaeger-pokemon-db --force --volumes
docker rm jaeger --force --volumes
 
docker volume rm node-jaeger-db-volume
docker volume rm node-jaeger-volume

docker image rm jaeger-node-mysql-demo
docker image rm jaeger-pokemon-db

docker network rm node-jaeger-network