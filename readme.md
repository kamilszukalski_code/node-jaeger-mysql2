# Findings

This project was to about what is Jaeger (tool/ui for tracing) as well as check how auto-instrumentation in opentelemetry works. Auto-instrumentations are plugins which role is to "automaticly" handle creation of spans - without enforcing user to manualy mark all operations on db, kafka etc. Great idea and it works but documentation for opentelemtry is terrible. It took me almost a week to fully grasp what pieces should be setup and how to do it. In the end however, I have managed to create bundle with working code.

# Setup in local development mode

All settings for local development are placed in `.env.local` file.

1. Run `docker-compose up db jaeger`
2. Run `npm run start`
3. You can one modify `src/demo.ts` and you will see changes in real time
4. To see all traces in ui, visit http://localhost:16686/
5. Db is accessible locally with settings in `.env.local`

# Setup in production mode

All settings for production development are placed in `.env` file. All pieces are delivered as docker images (including demo app). Please however remember that after each change in projec, one has to rebuild changed images (most of the times `node-demo`)

1. Run `docker-compose up`
2. To see all traces in ui, visit http://localhost:16686/
3. Db is accessible locally with settings in `.env.local`
4. You can check the size of produced image with `docker images | grep "jaeger"` command

# Stop and clear

To stop and clear all pieces of infrastrucutre createed for docker run script
`./docker/clear-entire-infrastructure.sh`

## ESBuild

With ESBuild I was able to create bundle which will be working with auto-instrumentation... sortof... Unfortunatly due to async_hook, libs used by instrumentations (in this case <b>mysql2</b> and <b>knex</b> if you use it) has to be marked as externals - which means that they have to be installed manually with `npm i`. Still it gives some benefits in image size:

- without bundling anything, image size is around: 328MB
- without bundling mysql2, image size is around: 152MB
- without bundling mysql2 and knex, image size is around: 156MB

There is included section in `/docker/demo/Dockerfile` file, where one can create unbundled image. However in 99% of a time we want as small size of image
as it is possible so it is commented. Feel free to uncomment it and check size of image in such setup.

## Prettier

Fixxing styling on save:

- Go to File > Preferences> Settings
- On your right-hand side, there is an icon to Open Settings in JSON format. Click on that icon.
- ```
  "editor.codeActionsOnSave": { "source.fixAll.eslint": true },
  "editor.formatOnSave": true,
  "eslint.alwaysShowStatus": true,
  "files.autoSave": "onFocusChange"
  ```

- https://medium.com/how-to-react/config-eslint-and-prettier-in-visual-studio-code-for-react-js-development-97bb2236b31a

## Webpack

As of 25.10.2021 I was not able to integrate instrumentations while running in webpack build. This is probably casued by the fact that webpack replaces regular require() command with its own.

https://github.com/open-telemetry/opentelemetry-js/issues/2475

Below are commands which I was trying to use as well as I have left webpack config which I was using.

```

```
