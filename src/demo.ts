/**
 * !!! VERY IMPORTANT !!!
 * For some reason, you have to load already initialized tracer before importing mysql/knex/kafka etc.
 * Otherwise, manual spans will work as expected - but all instrumentations WILL NOT WORK.
 * I hope this will help others, as it took me 1 day to figure out why it was not working.
 */
import {createConnection, RowDataPacket} from 'mysql2/promise';
import {context, trace} from '@opentelemetry/api';
import opentelemetry from '@opentelemetry/api';
import {knexMariaDbClient} from './common/db/knex';
import {sleep} from './common/utils/process-utils';

interface PokemonsTableRow extends RowDataPacket {
  name: string;
  id: number;
}

const tracer = opentelemetry.trace.getTracer('mysql-example');
const MAX_RETRY = 10;

const queryDatabase = async () => {
  const connectionPromise = await createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    port: Number(process.env.DB_NAME),
  });

  const queryPromise = 'SELECT name FROM Pokemons where id = 4';
  const [rows] = await connectionPromise.query<PokemonsTableRow[]>(queryPromise);
  console.log(`${queryPromise}: ${rows[0].name}`);

  const s = tracer.startSpan('other-heavy-job');
  console.log('Sleeping 2 seconds to simulate some other operations');
  await sleep(2000);
  console.log('Long hard job done!');
  s.end();
};

const queryDatabaseWithKnex = async () => {
  const result: PokemonsTableRow = await knexMariaDbClient('Pokemons')
    .where({
      id: 1,
    })
    .select('name', 'id')
    .first();

  console.log(`Result from knex ${result.name}`);
};

const retryUntilDbIsReady = async (action: () => Promise<void>) => {
  let i = 0;
  while (i < MAX_RETRY) {
    try {
      await sleep(1000 * i);
      await action();
      break;
    } catch (e) {
      console.log(`Attempt: ${i} Something went wrong during connection with db...`, e);
      i++;
    }
  }
};

const main = async () => {
  await retryUntilDbIsReady(queryDatabase);
  await retryUntilDbIsReady(queryDatabaseWithKnex);
};

const traceAndRun = async (name: string, main: () => Promise<void>) => {
  const span = tracer.startSpan(name);
  const ctx = trace.setSpan(context.active(), span);
  await context.with(ctx, main);
  span.end();

  console.log(`Visit http://localhost:16686/ to see produced traces for ${process.env.JAEGER_SERVICE_NAME} service.`);
};

traceAndRun('main-routine-inline', main).catch(console.error);
