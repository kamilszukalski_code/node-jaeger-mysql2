import opentelemetry from '@opentelemetry/api';
import {JaegerExporter} from '@opentelemetry/exporter-jaeger';
import {registerInstrumentations} from '@opentelemetry/instrumentation';
import MySQL2Instrumentation from '@opentelemetry/instrumentation-mysql2';
import {Resource} from '@opentelemetry/resources';
import {SimpleSpanProcessor} from '@opentelemetry/sdk-trace-base';
import {NodeTracerProvider} from '@opentelemetry/sdk-trace-node';
import {SemanticResourceAttributes} from '@opentelemetry/semantic-conventions';

const startOpenTelemetry = () => {
  const provider = new NodeTracerProvider({
    resource: new Resource({
      [SemanticResourceAttributes.SERVICE_NAME]: process.env.JAEGER_SERVICE_NAME || 'demo',
    }),
  });

  // sends output to the console
  // provider.addSpanProcessor(new BatchSpanProcessor(new ConsoleSpanExporter()));

  // sends output directly to jaeger
  provider.addSpanProcessor(
    new SimpleSpanProcessor(
      new JaegerExporter({
        endpoint: process.env.JAEGER_ENDPOINT,
      })
    )
  );

  // Initialize the provider
  provider.register();

  registerInstrumentations({
    instrumentations: [new MySQL2Instrumentation()],
    tracerProvider: provider,
  });

  return opentelemetry.trace.getTracer('mysql-example');
};

export const tracer = startOpenTelemetry();
