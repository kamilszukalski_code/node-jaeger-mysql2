const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    tracer: './src/tracer.ts',
    demo: './src/demo.ts',
  },
  target: 'node',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js',
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        loader: 'ts-loader',
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader',
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve('node_modules/jaeger-client/dist/src/jaeger-idl/thrift/jaeger.thrift'),
          to: 'jaeger-idl/thrift/jaeger.thrift',
        },

        {
          from: path.resolve('node_modules/@opentelemetry/instrumentation-mysql2'),
          to: '@opentelemetry/instrumentation-mysql2',
        },
      ],
    }),
  ],
  externals: [
    {
      // Possible drivers for knex - we'll ignore them
      // comment the one YOU WANT to use
      sqlite3: 'sqlite3',
      // mysql2: 'mysql2', // << using this one
      mariasql: 'mariasql',
      mysql: 'mysql',
      mssql: 'mssql',
      oracle: 'oracle',
      'strong-oracle': 'strong-oracle',
      oracledb: 'oracledb',
      pg: 'pg',
      tedious: 'tedious',
      'pg-query-stream': 'pg-query-stream',
      cardinal: 'cardinal',
      '@opentelemetry/instrumentation-mysql2/MySQL2Instrumentation': 'commonjs @opentelemetry/instrumentation-mysql2/MySQL2Instrumentation',
    },
  ],
};
