const path = require('path');
const fs = require('fs-extra');

const outputFolder = '/bundle';
const outputDir = path.join(__dirname, outputFolder);

fs.removeSync(outputDir);

require('esbuild')
  .build({
    entryPoints: ['src/tracer.ts'],
    bundle: true,
    outdir: outputDir,
    platform: 'node',
    target: 'node16.10',
    external: ['knex', 'mysql2'],
    minify: true,
  })
  .catch(() => process.exit(1));

// Copy file from node modules jaeger-client package - known "bug" that it has to be copied manually...
fs.copySync(path.join(__dirname, 'node_modules/jaeger-client/dist/src/jaeger-idl/thrift/jaeger.thrift'), path.join(__dirname, `${outputFolder}/jaeger-idl/thrift/jaeger.thrift`));

require('esbuild')
  .build({
    entryPoints: ['src/demo.ts'],
    bundle: true,
    outdir: outputDir,
    platform: 'node',
    target: 'node16.10',
    external: ['knex', 'mysql2'],
    minify: true,
  })
  .catch(() => process.exit(1));
