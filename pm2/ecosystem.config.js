const path = require('path');
module.exports = {
  apps: [
    {
      name: 'demo-prod',
      script: './bundle/demo.js',
      autorestart: false,
      instances: 1,
      combine_logs: false,
      node_args: '-r ./bundle/tracer.js',
      out_file: path.join(__dirname + '/../logs/demo.log'),
      error_file: path.join(__dirname + '/../logs/demo.errors.log'),
      log_date_format: 'YYYY-MM-DD HH:mm:ss sss Z',
      exec_mode: 'fork',
      env: {
        NODE_ENV: 'production',
      },
    },

    {
      name: 'demo-local',
      script: './bundle/demo.js',
      autorestart: false,
      instances: 1,
      combine_logs: false,
      out_file: path.join(__dirname + '/../logs/demo.log'),
      error_file: path.join(__dirname + '/../logs/demo.errors.log'),
      node_args: '-r dotenv/config -r ./bundle/tracer.js',
      args: ['dotenv_config_path=./.env.local'],
      exec_mode: 'fork',
      log_date_format: 'YYYY-MM-DD HH:mm:ss sss Z',
      env: {
        NODE_ENV: 'development',
      },
    },

    {
      name: 'demo-nobundle',
      script: './build/demo.js',
      autorestart: false,
      instances: 1,
      combine_logs: false,
      out_file: path.join(__dirname + '/../logs/demo.log'),
      error_file: path.join(__dirname + '/../logs/demo.errors.log'),
      node_args: '-r dotenv/config -r ./build/tracer.js',
      args: ['dotenv_config_path=./.env.local'],
      exec_mode: 'fork',
      log_date_format: 'YYYY-MM-DD HH:mm:ss sss Z',
      env: {
        NODE_ENV: 'development',
      },
    },
  ],
};
